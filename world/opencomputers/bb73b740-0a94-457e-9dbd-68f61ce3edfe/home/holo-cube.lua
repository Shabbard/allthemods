local component = require("component")
local hologram = component.hologram

local MAX_X = 48
local MAX_Y = 32
local MAX_Z = 48

hologram.clear()

hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, 0, 0, MAX_Y, 1)
hologram.fill(0, MAX_Z, 0, MAX_Y, 1)
hologram.fill(0, 0, 0, MAX_Y, 1)

hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)

hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
hologram.fill(MAX_X, MAX_Z, 0, MAX_Y, 1)
