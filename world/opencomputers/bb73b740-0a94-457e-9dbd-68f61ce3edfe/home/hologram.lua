local component = require("component")
local hologram = component.hologram

local MAX_X = 48
local MAX_Y = 32
local MAX_Z = 48

for x = 0, MAX_X do
 for y = 0, MAX_Y do
    for z = 0, MAX_Z do
     hologram.set(x, y, z, 1) 
    end
  end
end