local component = require("component")
local hologram = component.hologram
local args = {...}
hologram.set(tonumber(args[1]), tonumber(args[2]), tonumber(args[3]), args[4] == "true" or  args[4] == "on")